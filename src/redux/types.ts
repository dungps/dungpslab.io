export enum AppReducerType {}

export enum ReduxStateType {
  INIT = "init",
  LOADING = "loading",
  SUCCESS = "success",
  ERROR = "error",
  CANCELLED = "cancelled"
}

export interface ReduxData<T> {
  data: T
  status: ReduxStateType
  error?: Error
}

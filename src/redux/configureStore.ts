import { createStore, applyMiddleware, compose, Store } from "redux"
import middlewares from "./middlewares"
import reducers from "./reducers"
import { setupMiddleware } from "./middlewares"

export default () => {
  const store: Store = createStore(reducers, compose(applyMiddleware(...middlewares)))

  setupMiddleware()

  return store
}

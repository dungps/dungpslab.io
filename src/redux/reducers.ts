import { combineReducers } from "redux"

const reducers = combineReducers({})

export default reducers

export type AppState = ReturnType<typeof reducers>

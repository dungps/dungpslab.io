import React, { Fragment, useState, useEffect } from "react"
import { History } from "history"
import Helmet from "react-helmet"
import { Spinner } from "../components"
import AppRouter from "routers"
import ogImage from "assets/images/og-img.jpg"

interface Props {
  history: History
}

const App = (props: Props) => {
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    setTimeout(() => {
      setLoading(false)
    }, 1000)
  })

  console.log(window.location)

  return (
    <Fragment>
      <Helmet
        htmlAttributes={{ lang: "vi" }}
        defaultTitle="Kevin Pham"
        titleTemplate="%s - Kevin Pham"
      >
        <meta property="og:url" content={window.location.href} />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="A Kevin's website" />
        <meta
          property="og:image"
          content={`${window.location.protocol}//${window.location.host}${ogImage}`}
        />
        <meta property="og:site_name" content="Kevin Pham" />
        <meta name="description" content="A Kevin's website" />
      </Helmet>
      {loading ? <Spinner /> : <AppRouter history={props.history} />}
    </Fragment>
  )
}

export default App

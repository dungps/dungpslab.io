import React from "react"
import App from "./App"
import { History } from "history"
import { Provider } from "react-redux"

interface Props {
  store: any
  history: History
}

const Root = (props: Props) => {
  const { store, history } = props

  return (
    <Provider store={store}>
      <App history={history} />
    </Provider>
  )
}

export default Root

import React from "react"
import { Link } from "react-router-dom"
import styles from "./style.module.scss"
import siteRouter from "routers/site"

interface Props {
  match: any
}

const Menu = (props: Props) => {
  return (
    <ul className={`nav ${styles.appMenu}`}>
      {siteRouter.map((prop, key) => {
        if (!prop.label) return null

        return (
          <li className="nav-item" key={key}>
            <Link
              className={`nav-link ${props.match.path === prop.path ? styles.active : ""}`}
              to={prop.path}
            >
              {prop.label}
            </Link>
          </li>
        )
      })}
    </ul>
  )
}

export default Menu

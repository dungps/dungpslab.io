import React from "react"
import { Link } from "react-router-dom"
import { Menu } from "../Menu"
import headerImage from "assets/images/header.jpg"
import avatarImage from "assets/images/avatar.jpg"
import styles from "./style.module.scss"

const Header = (props: any) => {
  return (
    <header
      id="masthead"
      className={styles.siteHeader}
      role="banner"
      style={{ backgroundImage: `url(${headerImage})` }}
    >
      <Menu {...props} />
      <div className={styles.siteTitle}>
        <div className={styles.siteTitleInner}>
          <Link to="/" rel="home" className="site-brand">
            <img src={avatarImage} alt="Kevin Pham" width="120" height="120" />
            <h1>Kevin Pham</h1>
          </Link>
        </div>
      </div>
      <div className={styles.headerOverlay} />
    </header>
  )
}

export default Header

import React, { Fragment } from "react"
import { Switch, Route } from "react-router"
import siteRouter from "routers/site"
import { Header } from "./components/Header"
import styles from "./style.module.scss"
import { View404 } from "views/sites/404"

const MainLayout = (props: any) => {
  return (
    <Fragment>
      <Header {...props} />
      <main className={styles.siteMain}>
        <div className="mt-3">
          <Switch>
            {siteRouter.map((prop, key) => (
              <Route key={key} path={prop.path} component={prop.component} exact={prop.exact} />
            ))}
            <Route component={View404} />
          </Switch>
        </div>
      </main>
    </Fragment>
  )
}

export default MainLayout

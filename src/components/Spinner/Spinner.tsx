import React from "react"

const Spinner = () => {
  return (
    <div className="d-flex justify-content-center align-items-center w-100 vh-100">
      <div className="spinner-grow text-primary" />
    </div>
  )
}

export default Spinner

export default [
  {
    icon: "fab fa-facebook",
    title: "Facebook",
    href: "https://www.facebook.com/kevinpham9x",
    color: "#3b5998",
    options: {
      rel: "nofollow noopener noreferrer",
      target: "_blank"
    }
  },
  {
    icon: "fab fa-linkedin",
    title: "Linkedin",
    href: "https://www.linkedin.com/in/kevinpham93",
    color: "#0b66c2",
    options: {
      rel: "nofollow noopener noreferrer",
      target: "_blank"
    }
  },
  {
    icon: "fab fa-instagram",
    title: "Instagram",
    href: "https://www.instagram.com/orycdung",
    color: "#262626",
    options: {
      rel: "nofollow noopener noreferrer",
      target: "_blank"
    }
  },
  {
    icon: "fa fa-envelope",
    title: "Mail",
    href: "mailto:hi@dungps.com",
    color: "#dc493d",
    options: {}
  }
]

import React, { Fragment } from "react"
import homeConfig from "config/HomeConfig"
import Helmet from "react-helmet"

const ViewHome = () => {
  return (
    <Fragment>
      <Helmet title="Home" />
      <div className="row">
        {homeConfig.map((item, key) => (
          <div className="col-md-6 mt-3" key={key}>
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-4">
                    <i className={`fa-3x ${item.icon}`} style={{ color: item.color }} />
                  </div>
                  <div className="col-md-8 align-items-center d-flex">
                    <a href={item.href} {...item.options}>
                      {item.title}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </Fragment>
  )
}

export default ViewHome

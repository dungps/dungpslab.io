import React, { Fragment } from "react"
import { Link } from "react-router-dom"
import Helmet from "react-helmet"

const View404 = () => {
  return (
    <Fragment>
      <Helmet title="Page not found" />
      <h1>Page not found</h1>
      <p className="alert alert-info">
        Không tìm thấy trang. Quay trở lại <Link to="/">Home</Link>
      </p>
    </Fragment>
  )
}

export default View404

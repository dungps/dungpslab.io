import React, { Fragment } from "react"
import moment from "moment"
import showdown from "showdown"
import styles from "./style.module.scss"
import posts from "contents/_posts"
import Post from "models/Post"
import Helmet from "react-helmet"

interface Props {
  match: any
}

const converter = new showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
})

const ViewSingle = (props: Props) => {
  const post: Post | undefined = posts.find((p: Post) => p.slug.trim() === props.match.params.slug)

  if (!post) {
    return <p className="alert alert-danger mt-3 text-center">Không tìm thấy bài viết.</p>
  }

  return (
    <Fragment>
      <Helmet title={post.label} />
      <article className={styles.post}>
        <header>
          <div className={styles.entryMeta}>
            <span>{moment(post.createdAt, "x").format("MMM D, YYYY")}</span>
          </div>
          <h2 className={styles.entryTitle}>{post.label}</h2>
        </header>
        {post.image && (
          <div className={styles.entryThumbnail}>
            <img src={post.image} alt={post.label} width="800" height="530" />
          </div>
        )}
        <div className={styles.entryContent}>
          {post.content && (
            <p dangerouslySetInnerHTML={{ __html: converter.makeHtml(post.content) }}></p>
          )}
        </div>
      </article>
    </Fragment>
  )
}

export default ViewSingle

import React, { Fragment } from "react"
import moment from "moment"
import { Link } from "react-router-dom"
import posts from "contents/_posts"
import Post from "models/Post"
import styles from "./style.module.scss"
import Helmet from "react-helmet"

const ViewBlog = () => {
  if (posts.length < 1) {
    return <p className="alert alert-warning mt-3 text-center">Không có bài viết nào cả.</p>
  }

  return (
    <Fragment>
      <Helmet title="Blog" />
      {posts.map((post: Post, key: any) => (
        <article key={key} className={styles.post}>
          <header>
            <div className={styles.entryMeta}>
              <span>{moment(post.createdAt, "x").format("MMM D, YYYY")}</span>
            </div>
            <h2 className={styles.entryTitle}>
              <Link to={`/post/${post.slug}`} rel="bookmark" title={post.label}>
                {post.label}
              </Link>
            </h2>
          </header>
          {post.image && (
            <div className={styles.entryThumbnail}>
              <img src={post.image} alt={post.label} width="800" height="530" />
            </div>
          )}
          <div className={styles.entryContent}>
            {post.except && <p>{post.except}</p>}
            <Link
              to={`/post/${post.slug}`}
              rel="bookmark"
              className={styles.moreLink}
              title={post.label}
            >
              Continue reading <span className="meta-nav">→</span>
            </Link>
          </div>
        </article>
      ))}
    </Fragment>
  )
}

export default ViewBlog

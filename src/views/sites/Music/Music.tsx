import React, { Fragment } from "react"
import musicConfig from "config/MusicConfig"
import Helmet from "react-helmet"

const ViewMusic = () => {
  return (
    <Fragment>
      <Helmet title="Music" />
      <div className="text-center">
        <h1>{musicConfig.label}</h1>
        <div className="d-flex justify-content-center align-items-center mt-5">
          <iframe
            width="560"
            height="315"
            src={musicConfig.url}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            title="Youtube"
          />
        </div>
      </div>
    </Fragment>
  )
}

export default ViewMusic

interface Post {
  label: string
  slug: string
  content: string
  image?: any
  createdAt: number
  except?: string
}

export default Post

import Post from "models/Post"

export default [
  {
    label: "Ngưng suy nghĩ",
    slug: "ngung-suy-nghi",
    content: require("./ngung-suy-nghi").default,
    createdAt: 1584156896000
  }
] as Post[]

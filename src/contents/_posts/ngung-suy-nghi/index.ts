export default `
Có bao giờ bạn tự hỏi bản thân mình là tối hôm qua mình đã ăn những gì?<br/>
Có tất cả bao nhiêu món đang có trên bàn ăn?<br/>
Hình thù và mùi vị của món đó như thế nào?<br/>
Món đó vị thế nào?<br/>
Vị nó ra sao?<br/>
Chua, cay mặn hay là ngọt?<br/>
Nếu là vị ngọt thì cảm giác của vị ngọt đó như thế?<br/>

Nếu như bạn trả lời được hết câu hỏi trên thì chắc hẳn bạn đã có một bữa ăn ngon và đúng nghĩa là một bữa ăn.

Vậy nếu bạn không trả lời được những câu hỏi đó thì câu hỏi tiếp theo là, vậy lúc đó bạn đang làm gì? Có phải bạn đang liên tưởng về quá khứ, và tiếc nuối về nó; hay là bạn suy nghĩ về những dự định trong tương lai và lo lắng rằng mình có làm được những dự định đó hay không; hay là bạn đang mơ tưởng về những viễn cảnh không có thật.
`

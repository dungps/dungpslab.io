import { MainLayout } from "components"

const mainRouters = [
  {
    path: "/*",
    layout: MainLayout,
    exact: true
  }
]

export default mainRouters

import React from "react"
import { History } from "history"
import { Router, Switch, Route } from "react-router"
import mainRouters from "./main"

interface Props {
  history: History
}

const AppRouter = (props: Props) => {
  return (
    <Router history={props.history}>
      <Switch>
        {mainRouters.map((props, key) => (
          <Route key={key} path={props.path} component={props.layout} exact={props.exact} />
        ))}
      </Switch>
    </Router>
  )
}

export default AppRouter

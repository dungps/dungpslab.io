import { ViewHome } from "views/sites/Home"
import { ViewMusic } from "views/sites/Music"
import { ViewBlog, ViewSingle } from "views/sites/Blog"

const siteRouter = [
  {
    path: "/",
    component: ViewHome,
    exact: true,
    label: "Home"
  },
  {
    path: "/music",
    component: ViewMusic,
    label: "Music"
  },
  {
    path: "/blog",
    component: ViewBlog
    // label: "Blog"
  },
  {
    path: "/post/:slug",
    component: ViewSingle
  }
]

export default siteRouter
